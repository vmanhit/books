import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';

const Home = (props) => {
  return (
    <View style={styles.container}>
      <Text
        onPress={() => {
          props.navigation.navigate('DetailScreen');
        }}>
        Home
      </Text>
    </View>
  );
};

export default Home;
