import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';

import TabNavigator from './tabNavigator';

export default function Navigator() {
  return (
    <NavigationContainer>
      <TabNavigator />
    </NavigationContainer>
  );
}
